Simple fixed point library.

Main differences from https://github.com/johnmcfarlane/fixed_point :

1. Code is easier to read, no deep layers of metaprogramming. But less configurable.
2. Multiplication, division and conversions perform rounding to nearest representable value instead of truncation.