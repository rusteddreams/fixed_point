#pragma once

//              Copyright Ryhor Spivak 2016.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include <cstdint>
#include <cassert>
#include <type_traits>
#include <algorithm>
#include <utility>

namespace fixed_point
{
	// namespace for implementation details, shouldn't be used directly
	namespace detail
	{
		template<typename T>
		struct t_larger;
		template<> struct t_larger<int8_t> { using type = int16_t; };
		template<> struct t_larger<int16_t> { using type = int32_t; };
		template<> struct t_larger<int32_t> { using type = int64_t; };
		template<> struct t_larger<uint8_t> { using type = uint16_t; };
		template<> struct t_larger<uint16_t> { using type = uint32_t; };
		template<> struct t_larger<uint32_t> { using type = uint64_t; };
		template<typename T>
		using larger = typename t_larger<T>::type;

		// division with rounding to nearest integer
		template<typename T>
		constexpr T div_round( T a, T b )
		{
			return ( a + ((a<0)^(b<0)?-b/2:b/2) )/b;
		}

		static_assert( 
			div_round( 130, 30 ) == 4 && div_round( -130, -30 ) == 4 && div_round( -130, 30 ) == -4 && div_round( 130, -30 ) == -4 &&
			div_round( 120, 30 ) == 4 && div_round( -120, -30 ) == 4 && div_round( -120, 30 ) == -4 && div_round( 120, -30 ) == -4 &&
			div_round( 110, 30 ) == 4 && div_round( -110, -30 ) == 4 && div_round( -110, 30 ) == -4 && div_round( 110, -30 ) == -4, 
			"div_round correctness checks" );

		template<int E, typename T, typename std::enable_if_t<(E<0), int> = 0>
		constexpr T apply_exp( T x )
		{
			return detail::div_round(x,T(1)<<(-E));
		}

		template<int E, typename T, typename std::enable_if_t<(E>=0), int> = 0>
		constexpr T apply_exp( T x )
		{
			return x*(T(1)<<(E));	// extra () around E to silence msvc warning
		}

		template<int E, typename F, typename std::enable_if_t<(E<0), int> = 0>
		constexpr F apply_exp_f( F x )
		{
			return x*(F(1)/(1<<-E));
		}

		template<int E, typename F, typename std::enable_if_t<(E>=0), int> = 0>
		constexpr F apply_exp_f( F x )
		{
			return x*(1<<E);
		}
	}

	template<typename T, int E>
	struct fixed
	{
		fixed(){}

		// init from data
		constexpr fixed( T data, int )
			: data_( data )
		{}

		static constexpr fixed from_data( T x )
		{
			return fixed( x, 0 );
		}

		template<typename I, typename std::enable_if_t<std::is_integral<I>::value, int> = 0 >
		explicit constexpr fixed( I i )
			: data_( T( detail::apply_exp<-E>( i ) ) )
		{}

		template<typename F, typename std::enable_if_t<std::is_floating_point<F>::value, int> = 0 >
		explicit constexpr fixed( F f )
			: data_( T( detail::apply_exp_f<-E>(f) + (f>0?F(0.5):F(-0.5)) ) )
		{}

		template<typename T2, int E2>
		explicit constexpr fixed( fixed<T2,E2> f )
			: data_( detail::apply_exp<E2-E>( T(f.data_) ) )
		{}

		template<typename I, typename std::enable_if_t<std::is_integral<I>::value, int> = 0 >
		explicit constexpr operator I() const
		{
			return I( detail::apply_exp<E>( data_ ) );
		}

		template<typename F, typename std::enable_if_t<std::is_floating_point<F>::value, int> = 0 >
		explicit constexpr operator F() const
		{
			return detail::apply_exp_f<E>( F(data_) );
		}

		friend constexpr fixed operator-( fixed l )
		{
			return from_data( -l.data_ );
		}
		friend constexpr fixed operator+( fixed l, fixed r )
		{
			return from_data( l.data_ + r.data_ );
		}
		friend constexpr fixed operator-( fixed l, fixed r )
		{
			return from_data( l.data_ - r.data_ );
		}
		friend constexpr fixed operator*( fixed l, fixed r )
		{
			return from_data( T( detail::apply_exp<E>( detail::larger<T>(l.data_)*r.data_ ) ) );
		}
		friend constexpr fixed operator/( fixed l, fixed r )
		{
			return from_data( T( detail::div_round( detail::apply_exp<-E>( detail::larger<T>(l.data_) ), detail::larger<T>(r.data_) ) ) );
		}

		friend constexpr fixed operator+( int l, fixed r )
		{
			return fixed(l) + r;
		}
		friend constexpr fixed operator+( fixed l, int r )
		{
			return l + fixed(r);
		}
		friend constexpr fixed operator-( int l, fixed r )
		{
			return fixed(l) - r;
		}
		friend constexpr fixed operator-( fixed l, int r )
		{
			return l - fixed(r);
		}
		friend constexpr fixed operator*( int l, fixed r )
		{
			return from_data( T(l)*r.data_ );
		}
		friend constexpr fixed operator*( fixed l, int r )
		{
			return from_data( l.data_*T(r) );
		}
		friend constexpr fixed operator/( int l, fixed r )
		{
			return from_data( T( detail::div_round( detail::apply_exp<-E-E>(detail::larger<T>(l)), detail::larger<T>(r.data_) ) ) );
		}
		friend constexpr fixed operator/( fixed l, int r )
		{
			return from_data( detail::div_round( l.data_, T(r) ) );
		}

		friend constexpr fixed & operator+=( fixed & l, fixed r )
		{
			return l = l + r;
		}
		friend constexpr fixed & operator-=( fixed & l, fixed r )
		{
			return l = l - r;
		}
		friend constexpr fixed & operator*=( fixed & l, fixed r )
		{
			return l = l * r;
		}
		friend constexpr fixed & operator/=( fixed & l, fixed r )
		{
			return l = l / r;
		}

		friend constexpr fixed & operator+=( fixed & l, int r )
		{
			return l = l + r;
		}
		friend constexpr fixed & operator-=( fixed & l, int r )
		{
			return l = l - r;
		}
		friend constexpr fixed & operator*=( fixed & l, int r )
		{
			return l = l * r;
		}
		friend constexpr fixed & operator/=( fixed & l, int r )
		{
			return l = l / r;
		}

		friend constexpr bool operator<( fixed l, fixed r )
		{
			return l.data_ < r.data_;
		}
		friend constexpr bool operator>( fixed l, fixed r )
		{
			return l.data_ > r.data_;
		}
		friend constexpr bool operator<=( fixed l, fixed r )
		{
			return l.data_ <= r.data_;
		}
		friend constexpr bool operator>=( fixed l, fixed r )
		{
			return l.data_ >= r.data_;
		}
		friend constexpr bool operator==( fixed l, fixed r )
		{
			return l.data_ == r.data_;
		}
		friend constexpr bool operator!=( fixed l, fixed r )
		{
			return l.data_ != r.data_;
		}

		T data_;
	};

	namespace approx
	{
		// namespace for implementation details, shouldn't be used directly
		namespace detail
		{
#if defined _MSC_VER
			inline int log2_int( unsigned x )
			{
				assert( x );	// not 0
				unsigned long i;
				_BitScanReverse( &i, x );
				return int( i );
			}
#else
			inline int log2_int( unsigned x ) 
			{
				assert( x );	// not 0
				return 31 - __builtin_clz( x );
			}
#endif

			template <typename T, int E>
			fixed<T, E> sqrt_guess( fixed<T, E> x )
			{
				assert( x.data_ > T( 0 ) );
				int lg = log2_int( unsigned( x.data_ ) );
				auto d = fixed<T, E>::from_data( 1 << ((lg-E)/2) );
				// d*d <= x < d*d*4 and d <= sqrt(x) < d*2
				// return linear approximation of sqrt in this range
				return (x/d + d*2)/3;
			}

			template <typename T, int E>
			fixed<T, E> rsqrt_guess( fixed<T, E> x )
			{
				assert( x.data_ > T( 0 ) );
				int lg = log2_int( unsigned( x.data_ ) );
				auto d = fixed<T, E>::from_data( 1 << ((-E*3-lg)/2) );
				
				// order of multiplication is important for precision
				// don't use d*d*x here, if d is small then d*d will flush to zero
				auto dxd1 = d*x*d-1;
#define SQRT_2 1.4142135623730950488
				return d + d*std::max( dxd1*fixed<T, E>( 2.*(1.-SQRT_2) ), dxd1*fixed<T, E>( SQRT_2/2.-1. ) );
#undef SQRT_2
			}
		}

		template <unsigned iterations, typename T, int E>
		fixed<T, E> sqrt_newton( fixed<T, E> x )
		{
			if( x.data_ <= T( 0 ) )
				return fixed<T, E>( 0 );

			auto y = detail::sqrt_guess( x );
			for( unsigned i = 0; i != iterations; ++i )
				y = (y + x/y)/2;

			return y;
		}

		template <unsigned iterations, typename T, int E>
		fixed<T, E> rsqrt_newton( fixed<T, E> x )
		{
			if( x.data_ <= T( 0 ) )
				return fixed<T, E>( 0 );

			auto y = detail::rsqrt_guess( x );
			// order of multiplication is important for precision
			// don't use y*y*x here, if y is small then y*y will flush to zero
			for( unsigned i = 0; i != iterations; ++i )
				y = y*(3 - y*x*y)/2;

			return y;
		}

		// https://en.wikipedia.org/wiki/Methods_of_computing_square_roots#Goldschmidt.E2.80.99s_algorithm
		template <unsigned iterations, typename T, int E, typename std::enable_if_t<std::is_signed<T>::value, int> = 0>
		std::pair<fixed<T, E>, fixed<T, E>> sqrt_rsqrt_goldschmidt( fixed<T, E> x )
		{
			static_assert( iterations > 0, "At least one iteration is required for Goldschmidt" );
			if( x.data_ <= T( 0 ) )
				return { fixed<T, E>( 0 ), fixed<T, E>( 0 ) };

			auto y = detail::rsqrt_guess( x );
			x = x*y;
			auto h2 = y;

			for( unsigned i = 0; i != iterations; ++i )
			{
				auto r = (1 - x*h2)/2;	// r can be negative so we std::enable_if this function only for signed types
				x = x + x*r;
				h2 = h2 + h2*r;
			}

			return { x, h2 };
		}
	}
}
