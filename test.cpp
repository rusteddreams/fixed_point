#include <iostream>
#include <cmath>

#include "fixed.h"

template<typename T>
void normalize_test( T x )
{
	auto xx = x*x;
	auto r = fixed_point::approx::rsqrt_newton<3>( xx );
	auto n = x*r;
	std::cout << "normalize test:\n"
		"x =            " << static_cast<float>(x) << "\t[" << x.data_ << "]\n"
		"x*x =          " << static_cast<float>(xx) << "\t[" << xx.data_ << "]\n"
		"rsqrt(x*x) =   " << static_cast<float>(r) << "\t[" << r.data_ << "]\n"
		"x*rsqrt(x*x) = " << static_cast<float>(n) << "\t[" << n.data_ << "]\n"
		"\n";
}

void test()
{
	using namespace fixed_point;

	// very small values, flush to 0:
	normalize_test( fixed<int16_t,-8>::from_data( 1 ) );
	normalize_test( fixed<int16_t,-8>::from_data( 2 ) );
	// smallest value working correctly:
	normalize_test( approx::sqrt_newton<3>( fixed<int16_t,-8>::from_data( 1 ) ) );
	normalize_test( approx::sqrt_newton<3>( fixed<int16_t,-8>::from_data( 2 ) ) );
	normalize_test( approx::sqrt_newton<3>( fixed<int16_t,-8>::from_data( 32 ) ) );
	// big value, x*x is close to max fixed<int16_t,-8>:
	normalize_test( fixed<int16_t,-8>( 11.31 ) );

	std::cout 
		<< static_cast<float>( approx::sqrt_newton<0>( fixed<int32_t,-16>(15.5) ) ) << ' ' 
		<< static_cast<float>( approx::sqrt_newton<1>( fixed<int32_t,-16>(15.5) ) ) << ' ' 
		<< static_cast<float>( approx::sqrt_newton<2>( fixed<int32_t,-16>(15.5) ) ) << ' ' 
		<< static_cast<float>( approx::sqrt_newton<3>( fixed<int32_t,-16>(15.5) ) ) << ' ' 
		<< static_cast<float>( fixed<int32_t,-16>( sqrt(15.5) ) ) << '\n';

	std::cout 
		<< static_cast<float>( 1/approx::sqrt_newton<3>( fixed<int32_t,-16>(15.5) ) ) << ' ' 
		<< static_cast<float>( approx::rsqrt_newton<0>( fixed<int32_t,-16>(15.5) ) ) << ' ' 
		<< static_cast<float>( approx::rsqrt_newton<1>( fixed<int32_t,-16>(15.5) ) ) << ' ' 
		<< static_cast<float>( approx::rsqrt_newton<2>( fixed<int32_t,-16>(15.5) ) ) << ' ' 
		<< static_cast<float>( approx::rsqrt_newton<3>( fixed<int32_t,-16>(15.5) ) ) << ' ' 
		<< static_cast<float>( fixed<int32_t,-16>( 1/sqrt(15.5) ) ) << '\n';

	std::cout 
		<< static_cast<float>( approx::sqrt_rsqrt_goldschmidt<1>( fixed<int32_t,-16>(15.5) ).first ) << ' ' 
		<< static_cast<float>( approx::sqrt_rsqrt_goldschmidt<2>( fixed<int32_t,-16>(15.5) ).first ) << ' ' 
		<< static_cast<float>( approx::sqrt_rsqrt_goldschmidt<3>( fixed<int32_t,-16>(15.5) ).first ) << ' ' 
		<< static_cast<float>( fixed<int32_t,-16>( sqrt(15.5) ) ) << '\n';

	std::cout 
		<< static_cast<float>( approx::sqrt_rsqrt_goldschmidt<1>( fixed<int32_t,-16>(15.5) ).second ) << ' ' 
		<< static_cast<float>( approx::sqrt_rsqrt_goldschmidt<2>( fixed<int32_t,-16>(15.5) ).second ) << ' ' 
		<< static_cast<float>( approx::sqrt_rsqrt_goldschmidt<3>( fixed<int32_t,-16>(15.5) ).second ) << ' ' 
		<< static_cast<float>( fixed<int32_t,-16>( 1/sqrt(15.5) ) ) << '\n';
}

int main()
{
	try
	{
		test();
	}
	catch( ... )
	{
		std::cout << "exception\n";
		return 1;
	}
	return 0;
}
